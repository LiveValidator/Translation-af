# LiveValidator - Translation Afrikaans

[![build status](https://gitlab.com/LiveValidator/Translation-af/badges/master/build.svg)](https://gitlab.com/LiveValidator/Translation-af/commits/master)
[![coverage report](https://gitlab.com/LiveValidator/Translation-af/badges/master/coverage.svg)](https://gitlab.com/LiveValidator/Translation-af/commits/master)

A complete Afrikaans translation for all errors that should cover the following testers:
- [html5](https://gitlab.com/LiveValidator/Tester-html5)

Find the project [home page and docs](https://livevalidator.gitlab.io/).
