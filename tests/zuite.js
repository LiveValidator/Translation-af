/**
 * The test suite for testing that all messages are translated
 */
it( 'Is af complete', function() {
    var extend = LiveValidator.utils.extend(
        {},
        LiveValidator.translations[ 'en-us' ],
        LiveValidator.translations.af
    );

    // Check that all keys are filled
    for ( var key in extend ) {
        if ( LiveValidator.translations.af[ key ] ) {
            expect( extend[ key ] ).toEqual( LiveValidator.translations.af[ key ] );
        } else {
            fail( 'The following error is missing: ' + key );
        }
    }
} );
